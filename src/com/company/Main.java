package com.company;

import com.company.reconstructors.AbstractEndpointsReconstructor;
import com.company.reconstructors.NormalEndpointReconstructor;

import java.nio.file.Path;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<RestClass> sources = loadSources(args);
        new NormalEndpointReconstructor(sources).print();
        System.out.println();
        new AbstractEndpointsReconstructor(sources).print();
    }

    private static List<RestClass> loadSources(String... args) {
        if (args.length < 1 || args[0].isEmpty()) {
            throw new IllegalArgumentException("Please set path to sources as fist argument of program");
        }
        FilesIterator filesIterator = new FilesIterator(args[0]);
        FileHandler fileHandler = new FileHandler();
        filesIterator.iterate(fileHandler);

        for (Path path : fileHandler.getUncompilableFiles()) {
            System.err.println("Unable to compile:" + path.toString());
        }
        try {
            Thread.sleep(50l);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return fileHandler.getResults();
    }
}
