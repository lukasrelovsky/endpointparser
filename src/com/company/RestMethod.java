package com.company;

/**
 * Created by install on 2/17/2015.
 */
public class RestMethod {

    public String httpMethod;
    public String methodUrl;
    public String javaMethod;
    public String type;

    public String getMethodUrl() {
        if (methodUrl == null) {
            return "";
        }
        return methodUrl;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public void setMethodUrl(String methodUrl) {
        this.methodUrl = methodUrl;
    }

    public String getJavaMethod() {
        return javaMethod;
    }

    public void setJavaMethod(String javaMethod) {
        this.javaMethod = javaMethod;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String toOutput(RestClass clazz) {
        return getHttpMethod() + " " + clazz.getUrl() + getMethodUrl() + " " + clazz.getPackageName() + "." + clazz.getClassName() + "." + getJavaMethod();
    }

    public static String removeGenericsFromType(String type) {
        if (type.contains("<")) {
            type = type.substring(0, type.indexOf("<"));
        }
        return type;
    }

    @Override
    public String toString() {
        return "RestMethod{" +
                "httpMethod='" + httpMethod + '\'' +
                ", methodUrl='" + methodUrl + '\'' +
                ", javaMethod='" + javaMethod + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
