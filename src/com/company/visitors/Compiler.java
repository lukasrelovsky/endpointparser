package com.company.visitors;

import com.company.RestClass;
import japa.parser.ast.CompilationUnit;

import java.util.List;

/**
 * Created by install on 2/19/2015.
 */
public class Compiler {


    public static RestClass compile(CompilationUnit compilationUnit, List<String> lines) {
        ClassVisitor classVisitor = new ClassVisitor();
        MethodVisitor methodVisitor = new MethodVisitor();
        ImportVisitor importVisitor = new ImportVisitor();

        classVisitor.visit(compilationUnit, lines);
        methodVisitor.visit(compilationUnit, lines);
        importVisitor.visit(compilationUnit, lines);

        RestClass restClass = classVisitor.getRestClass();
        if (restClass == null) {
            return restClass;
        }
        restClass.setImports(importVisitor.getRestClass().getImports());
        restClass.setMethods(methodVisitor.getRestMethods());

        return restClass;
    }
}
