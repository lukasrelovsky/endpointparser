package com.company.visitors;


import com.company.RestMethod;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.Parameter;
import japa.parser.ast.expr.AnnotationExpr;
import japa.parser.ast.visitor.VoidVisitorAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by install on 2/17/2015.
 */
public class MethodVisitor extends VoidVisitorAdapter<List<String>> {


    private List<RestMethod> restMethods = new ArrayList<>();

    @Override
    public void visit(MethodDeclaration methodDeclaration, List<String> file) {
        if (methodDeclaration.getAnnotations() != null) {
            RestMethod restMethod = null;
            for (AnnotationExpr annotationExpr : methodDeclaration.getAnnotations()) {
                String annotation = annotationExpr.getName().getName();
                if ("GET".equals(annotation) ||
                        "PUT".equals(annotation) ||
                        "POST".equals(annotation) ||
                        "DELETE".equals(annotation) ||
                        "HEAD".equals(annotation)) {
                    if (restMethod == null) {
                        restMethod = new RestMethod();
                    }
                    restMethod.setHttpMethod(annotation);
                }
                if ("Path".equals(annotation)) {
                    if (restMethod == null) {
                        restMethod = new RestMethod();
                    }
                    String code = file.get(annotationExpr.getBeginLine() - 1);
                    String path = code.replace("@Path", "").replace("(", "").replace(")", "").replace("\"", "").trim();
                    restMethod.setMethodUrl(path);
                }
            }
            if (restMethod != null) {
                restMethod.setJavaMethod(methodDeclaration.getName() + "(" + getParameters(methodDeclaration.getParameters()) + ")");
                restMethod.setType(methodDeclaration.getType().toString());
                this.restMethods.add(restMethod);
            }
        }
    }

    public List<RestMethod> getRestMethods() {
        return restMethods;
    }

    private String getParameters(List<Parameter> params) {
        StringBuilder sb = new StringBuilder(100);
        if (params != null) {
            for (Parameter param : params) {
                sb.append(param.getType().toString() + " " + param.getId().toString() + ",");
                sb.append(" ");
            }
        }
        if (sb.length() > 0) {
            int lastSpace = sb.lastIndexOf(", ");
            sb.replace(lastSpace, lastSpace + 2, "");
        }
        return sb.toString();
    }


}
