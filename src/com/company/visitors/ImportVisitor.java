package com.company.visitors;

import com.company.RestClass;
import japa.parser.ast.ImportDeclaration;
import japa.parser.ast.visitor.VoidVisitorAdapter;

import java.util.List;

/**
 * Created by install on 2/19/2015.
 */
public class ImportVisitor extends VoidVisitorAdapter<List<String>> {


    private RestClass restClass = new RestClass();

    public RestClass getRestClass() {
        return restClass;
    }

    @Override
    public void visit(ImportDeclaration importDeclaration, List<String> list) {
        String imporLine = list.get(importDeclaration.getBeginLine() - 1);
        imporLine = imporLine.replaceFirst("import", "").replaceAll("\\;", "").trim();
        restClass.getImports().add(imporLine);
    }


}
