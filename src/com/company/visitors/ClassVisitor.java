package com.company.visitors;

import com.company.RestClass;
import japa.parser.ast.body.ClassOrInterfaceDeclaration;
import japa.parser.ast.expr.AnnotationExpr;
import japa.parser.ast.visitor.VoidVisitorAdapter;

import java.util.List;

/**
 * Created by install on 2/18/2015.
 */
public class ClassVisitor extends VoidVisitorAdapter<List<String>> {

    private RestClass restClass;

    public RestClass getRestClass() {
        return restClass;
    }


    @Override
    public void visit(ClassOrInterfaceDeclaration classOrInterfaceDeclaration, List<String> ls) {
        List<AnnotationExpr> annotations = classOrInterfaceDeclaration.getAnnotations();
        if (annotations != null) {
            if (isRestEndpoint(annotations, ls)) {
                restClass = new RestClass();
                restClass.setClassName(classOrInterfaceDeclaration.getName());
                restClass.setPackageName(getPackage(ls));
                restClass.setUrl(getUrl(annotations, ls));
            }
        }
    }

    /**
     * na zaklade vsetkcyh anotacii ktore maju Path mi vyberie vnutro
     *
     * @param annotations
     * @param list
     * @return
     */
    public String getUrl(List<AnnotationExpr> annotations, List<String> list) {
        for (AnnotationExpr annotationExpr : annotations) {
            if ("Path".equals(annotationExpr.getName().getName())) {
                String annotation = list.get(annotationExpr.getBeginLine() - 1);
                String annotationContents = parseAnnotationContents(annotation).replace("\"", "");
                return annotationContents;
            }
        }
        return "";
    }

    /**
     * vieme zeci je tam restovy endpoint
     *
     * @param annotations
     * @param list
     * @return
     */
    private boolean isRestEndpoint(List<AnnotationExpr> annotations, List<String> list) {
        for (AnnotationExpr annotationExpr : annotations) {

            if ("Produces".equals(annotationExpr.getName().getName())) {
                String content = parseAnnotationContents(list.get(annotationExpr.getBeginLine() - 1));
                if (content.contains("MediaType.APPLICATION_JSON")) {
                    return true;
                }
            }

            if ("Path".equals(annotationExpr.getName().getName())) {
                return true;
            }
        }
        return false;
    }


    /**
     * vyparsuje mi obsah anotacia
     *
     * @param annotation
     * @return
     */
    private String parseAnnotationContents(String annotation) {
        return annotation.substring(annotation.indexOf('(') + 1, annotation.indexOf(')'));
    }


    private String getPackage(List<String> lines) {
        for (String line : lines) {
            if (line.trim().startsWith("package")) {
                String[] split = line.split("package\\s");
                for (String s : split) {
                    if (!s.equals("")) {
                        String[] temp = s.split("\\;");
                        return temp[0];
                    }
                }
            }
        }
        throw new IllegalStateException("can't detected package");
    }

}
