package com.company.reconstructors;

import com.company.RestClass;
import com.company.RestMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by install on 2/23/2015.
 */
public class AbstractEndpointsReconstructor {


    private Map<String, RestClass> abstractEndpoints;
    private List<RestClass> unresolvedEndpoints;

    public AbstractEndpointsReconstructor(List<RestClass> sources) {
        abstractEndpoints = getAbstractEndpoints(sources);
        unresolvedEndpoints = getUnresolvedEndpoints(sources);
    }

    /**
     * From all endpoints print URL which are null and putting to Map with your getPackageName and getClassName
     *
     * @param endpoints
     * @return
     */
    private static Map<String, RestClass> getAbstractEndpoints(List<RestClass> endpoints) {
        Map<String, RestClass> result = new HashMap<>();
        for (RestClass endpoint : endpoints) {
            if ("".equals(endpoint.getUrl())) {
                result.put(endpoint.getFullName(), endpoint);
            }
        }
        return result;
    }

    /**
     * vsetky httpMethody ktore su null a k nim prisluchajuce metody
     *
     * @param endpoints
     * @return List RestClass s metodamy, packageName, url, importy
     */
    private static List<RestClass> getUnresolvedEndpoints(List<RestClass> endpoints) {
        List<RestClass> result = new ArrayList<>();
        for (RestClass endpoint : endpoints) {
            List<RestMethod> unresolvedMethods = new ArrayList<>();
            for (RestMethod restMethod : endpoint.getMethods()) {
                if (restMethod.getHttpMethod() == null) {
                    unresolvedMethods.add(restMethod);
                }
            }
            if (!unresolvedMethods.isEmpty()) {
                endpoint.setMethods(unresolvedMethods);
                result.add(endpoint);
            }
        }
        return result;
    }

    public void print() {
        for (RestClass unresolvedEndpoint : unresolvedEndpoints) {
            printEndpoint(unresolvedEndpoint, unresolvedEndpoint.getUrl(), unresolvedEndpoint.getFullName());
        }
    }

    private void printEndpoint(RestClass clazz, String urlPrefix, String javaPrefix) {
        for (RestMethod restMethod : clazz.getMethods()) {
            String methodType = clazz.findImport(RestMethod.removeGenericsFromType(restMethod.getType()));
            String url = urlPrefix + restMethod.getMethodUrl();
            String javaPath = String.format("%s.%s", javaPrefix, restMethod.getJavaMethod());
            if (abstractEndpoints.containsKey(methodType)) {
                printEndpoint(abstractEndpoints.get(methodType), url, javaPath);
            } else {
                print(restMethod.getHttpMethod(), url, javaPath);
            }
        }
    }


    private void print(String httpMethod, String url, String javaPath) {
        url = url.replaceAll("//", "/");
        System.out.printf("%s %s %s%n", httpMethod, url, javaPath);
    }

}
