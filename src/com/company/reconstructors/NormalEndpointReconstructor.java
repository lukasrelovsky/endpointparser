package com.company.reconstructors;

import com.company.RestClass;
import com.company.RestMethod;
import java.util.List;

/**
 * Created by install on 2/23/2015.
 */
public class NormalEndpointReconstructor {

    private List<RestClass> endpoints;

    public NormalEndpointReconstructor(List<RestClass> endpoints) {
        this.endpoints = endpoints;
    }

    /**
     * Print from all endpoint their HTTPmethod, url, package, className, method with input parameters
     */
    public void print() {
        for (RestClass endpoint : endpoints) {
            if (!"".equals(endpoint.getUrl())) {
                for (RestMethod restMethod : endpoint.getMethods()) {
                    String url = endpoint.getUrl() + restMethod.getMethodUrl();
                    String urlRep = url.replaceAll("//", "/");
                    if (restMethod.getHttpMethod() != null) {
                        System.out.printf("%s %s %s.%s%n", restMethod.getHttpMethod(), urlRep, endpoint.getFullName(), restMethod.getJavaMethod());
                    }
                }
            }
        }
    }

}
