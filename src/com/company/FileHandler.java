package com.company;

import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by install on 2/20/2015.
 */
public class FileHandler implements FilesIterator.FileHandler {

    private List<Path> uncompilableFiles = new ArrayList<>();
    private List<RestClass> results = new ArrayList<>();

    public List<Path> getUncompilableFiles() {
        return uncompilableFiles;
    }

    public List<RestClass> getResults() {
        return results;
    }

    @Override
    public void handle(Path path) {
        try {
            List<String> lines = Files.readAllLines(path, Charset.defaultCharset());
            CompilationUnit cu = getCompilationUnit(path.toFile());
            RestClass compiled = com.company.visitors.Compiler.compile(cu, lines);
            if (compiled != null) {
                results.add(compiled);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            uncompilableFiles.add(path);
        }
    }

    private static CompilationUnit getCompilationUnit(File file) throws ParseException {
        FileInputStream in = null;

        try {
            in = new FileInputStream(file);
            return JavaParser.parse(in);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(String.format("File:%s not found", file.getAbsolutePath()), e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    throw new IllegalArgumentException(String.format("Error while closing file:%s", file.getAbsolutePath()), e);
                }
            }
        }
    }
}