package com.company;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by install on 2/18/2015.
 */
public class RestClass {

    private List<RestMethod> methods = new ArrayList<>();
    private String packageName;
    private String className;
    private String url;
    private List<String> imports = new ArrayList<>();

    public List<RestMethod> getMethods() {
        return methods;
    }

    public void setMethods(List<RestMethod> methods) {
        this.methods = methods;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getImports() {
        return imports;
    }

    public void setImports(List<String> imports) {
        this.imports = imports;
    }

    public String getFullName() {
        return getPackageName() + "." + getClassName();
    }

    public String findImport(String clazz) {
        for (String anImport : imports) {
            String importedClass = anImport.substring(anImport.lastIndexOf(".") + 1);
            if (importedClass.equals(clazz)) {
                return anImport;
            }
        }
        return getPackageName() + "." + clazz;
    }

    @Override
    public String toString() {
        return "RestClass{" +
                "methods=" + methods +
                ", packageName='" + packageName + '\'' +
                ", className='" + className + '\'' +
                ", url='" + url + '\'' +
                ", imports=" + imports +
                '}';
    }
}
