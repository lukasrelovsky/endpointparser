package com.company;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by install on 2/18/2015.
 */
public class FilesIterator {

    private String baseDirectory;
    private FileHandler fileHandler;

    public FilesIterator(String baseDirectory) {
        this.baseDirectory = baseDirectory;
    }

    public void iterate(FileHandler fileHandler) {
        this.fileHandler = fileHandler;
        iterate(Paths.get(baseDirectory));
    }

    private void iterate(Path path) {
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path)) {
            for (Path element : directoryStream) {
                File file = element.toFile();
                if (file.isDirectory()) {
                    iterate(element);
                }
                if (file.isFile() && file.getName().endsWith(".java")) {
                    fileHandler.handle(element);
                }
            }
        } catch (IOException ex) {
            throw new IllegalArgumentException(String.format("Cannot open directory:%s", path), ex);
        }
    }

    public static interface FileHandler {

        public void handle(Path file);
    }

}
